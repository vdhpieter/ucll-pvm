`\visible<2>{$\rightarrow$}`int xs[5];

`\visible<3>{$\rightarrow$}`int* p = &(xs[0]);
`\visible<4>{$\rightarrow$}`int* q = xs;

`\visible<5>{$\rightarrow$}`p[2];
`\visible<6>{$\rightarrow$}`q[3];
`\visible<7>{$\rightarrow$}`p[5];
