\usepackage{../pvm}
\usepackage{fourier}
\usepackage{bbding}

\usetikzlibrary{shadows,shapes.multipart}

\title{Allocation Methods}
\author{Fr\'ed\'eric Vogels}




\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \frametitle{Overview}
  \begin{itemize}
    \item Different ways to deal with RAM
    \item Similar to data structures
    \item Trade flexibility for speed
    \item Normally abstracted away as much as possible
    \item In \cpp: crucial to know about the details
  \end{itemize}
\end{frame}

\section{Static Allocation}

\begin{frame}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}
  \frametitle{Static Allocation}
  \begin{itemize}
    \item Simplest allocation mechanism
    \item Fastest
    \item Most restrictive
    \item Used in oldest programming language \\ (Fortran, early 1950s)
  \end{itemize}
  \vskip1cm
  \begin{center}
    {\color{red}\Huge\danger}
    \parbox{8cm}{\centering 
      {\color{red}\Huge \textsc{warning}} \\
      Examples that follow are what-ifs \\
      They do not show how really \cpp\ works
    }
    {\color{red}\Huge\danger}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{How Does It Work?}
  \notrealcpp
  \begin{itemize}
    \item The compiler assigns a fixed address to each variable
  \end{itemize}
  \code[frame=none]{allocation.cpp}
  \begin{tikzpicture}[overlay,remember picture,allocation/.style={-latex,thick}]
    \coordinate (upperleft) at (6,4);

    \begin{scope}[scale=.5]
      \coordinate (double) at (upperleft);
      \coordinate (int) at ($ (upperleft) + (0,-1) $);
      \coordinate (char) at ($ (upperleft) + (4,-1) $);

      \visible<2->{
        \draw[fill=red!50] (double) rectangle ++(8,-1);
      }

      \visible<3->{
        \draw[fill=green!50] (int) rectangle ++(4,-1);
      }

      \visible<4->{
        \draw[fill=blue!50] (char) rectangle ++(1,-1);
      }

      \draw (upperleft) grid ++(8,-4);
    \end{scope}

    \visible<2>{
      \draw[allocation] (global) -- (double);
    }

    \visible<3>{
      \draw[allocation] (param) -- ($ (int) + (0,-0.25) $);
    }

    \visible<4>{
      \draw[allocation] (local) -- ($ (char) + (0,-0.25) $);
    }
  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Consequences of Static Allocation}
  \notrealcpp
  \begin{itemize}
    \item Compiler needs to have full knowledge at compile-time
  \end{itemize}
  \code{array-creation.cpp}
\end{frame}

\begin{frame}
  \frametitle{Consequences of Static Allocation}
  \notrealcpp
  \begin{itemize}
    \item Local variables ``remember'' their values
  \end{itemize}
  \code{static-locals.cpp}
\end{frame}

\begin{frame}
  \frametitle{Quick Intermezzo}
  \begin{itemize}
    \item You can actually make this work in \cpp
  \end{itemize}
  \code{static-locals-working.cpp}
\end{frame}


\begin{frame}
  \frametitle{Consequences of Static Allocation}
  \notrealcpp
  \begin{itemize}
    \item Recursion not possible
  \end{itemize}
  \code{static-recursion.cpp}
\end{frame}

\begin{frame}
  \frametitle{Static Allocation}
  \begin{procontralist}
    \pro Very fast
    \pro No bookkeeping needed at runtime
    \pro No out-of-memory errors possible
    \pro Entire memory footprint known beforehand
    \con Data structure size must be known at compile time
    \con Full recompilation needed to allow larger data structures
    \con Cannot make use of extra RAM:
         If it's been compiled to work with 1MB but you have 8GB, too bad
    \con No recursion possible
  \end{procontralist}
\end{frame}

\section{Stack Allocation}

\begin{frame}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}
  \frametitle{Stack Allocation}
  \begin{itemize}
    \item Allocate your variables using static allocation
    \item Introduce a {\tt new} keyword that allows for runtime allocation
    \item Take the remainder of RAM (what's left after static allocation)
    \item Act as if it were a stack of available bytes
    \item Allocation = take top $N$ bytes
  \end{itemize}
  \vskip5mm
  \begin{center}
    \begin{tikzpicture}
      \draw (0,0) rectangle ++(1,2);
      \draw (1,0) rectangle ++(7,2);

      \node[rotate=90] at (0.5,1) {static};
      \node[rotate=90] at (4.5,1) {stack};

      \draw[|-|] (0,-.5) -- ++(8,0) node[midway,below] {RAM};
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Stack Allocation}
  \notrealcpp
  \begin{columns}
    \column{5cm}
    \code[font size=\small,frame=none]{stack-allocation.cpp}
    \column{4cm}
    \begin{center}
      \begin{tikzpicture}[allocation/.style={-latex,thick},remember picture,overlay,scale=.5]
        \coordinate (start) at (-4,4);

        \only<handout:1-|3->{
          \draw[fill=red!50] (start) rectangle ++(4,-1);
        }
        \only<handout:1|3>{
          \draw[allocation] (var a) to [bend left=30] ($ (start) + (0,-0.5) $);
        }

        \only<handout:1-|4->{
          \draw[fill=green!50] ($ (start) + (4,0) $) rectangle ++(4,-1);
        }
        \only<handout:1|4>{
          \draw[allocation] (var n) to [bend left=30] ($ (start) + (4,-0.5) $);
        }

        \only<handout:1-|5->{
          \draw[fill=blue!50] ($ (start) + (0,-1) $) rectangle ++(4,-1);
        }
        \only<handout:1|5>{
          \draw[allocation] (var b) to [bend left=30] ($ (start) + (0,-1.5) $);
        }

        \only<handout:2|6->{
          \draw[fill=red!50] ($ (start) + (4,-1) $) rectangle ++(4,-1);
          \draw[fill=red!50] ($ (start) + (0,-2) $) rectangle ++(8,-2);
        }
        \only<handout:2|6>{
          \draw[allocation] (mem a) to [bend left=30] ($ (start) + (4,-1.5) $);
        }

        \only<handout:2|8->{
          \draw[fill=green!50] ($ (start) + (0,-4) $) rectangle ++(8,-3);
        }
        \only<handout:2|8>{
          \draw[allocation] (mem b) to [bend right=30] ($ (start) + (0,-4.5) $);
        }

        \draw (start) grid ++(8,-9);
      \end{tikzpicture}
    \end{center}
  \end{columns}
  \vskip2cm
  \begin{overprint}
    \onslide<handout:1|2-5>
    \begin{center}
      \textbf{Compilation stage} \\ 
      Compiler assigns memory locations to each variable \\
      (this is still static allocation)
    \end{center}
    \onslide<handout:2|6>
    \begin{center}
      \textbf{At runtime} \\ Extra memory is allocated on stack at runtime
    \end{center}
    \onslide<handout:0|7>
    \begin{center}
      \textbf{At runtime} \\ Say user enters 3
    \end{center}
  \end{overprint}
\end{frame}

\begin{frame}
  \frametitle{Stack Allocation}
  \begin{itemize}
    \item You can keep on allocating\dots
    \item \dots but eventually you'll run out of memory (stack overflow)
    \item Why not deallocate unused memory?
    \item Problem: can only deallocate what's on top of the stack
  \end{itemize}
  \code[font size=\small,width=6cm]{stack-deallocation.cpp}
\end{frame}

\begin{frame}
  \frametitle{Recursion Becomes Possible}
  \code{recursion.cpp}
  \begin{center}
    \begin{tikzpicture}[scale=.5,transform shape,
                        stack frame/.style={anchor=north west,minimum width=4cm,minimum height=1cm,fill=red,opacity=.75,text opacity=1}]
      \draw (0,0) grid ++(4,-8);

      \only<2-12>{
        \node[stack frame] at (0,0) {n=5};
      }

      \only<3-11>{
        \node[stack frame] at (0,-1) {n=4};
      }

      \only<4-10>{
        \node[stack frame] at (0,-2) {n=3};
      }

      \only<5-9>{
        \node[stack frame] at (0,-3) {n=2};
      }

      \only<6-8>{
        \node[stack frame] at (0,-4) {n=1};
      }

      \only<7>{
        \node[stack frame] at (0,-5) {n=0};
      }
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Stack Allocation}
  \begin{procontralist}
    \pro Fast allocation
    \pro Little runtime bookkeeping needed
    \pro Amount of memory to be allocated does not have to be known at compile time
    \pro Can allocate any amount of memory you want at runtime
    \pro Recursion made possible
    \con Problematic deallocation
  \end{procontralist}
\end{frame}

\section{Heap Allocation}

\begin{frame}
  \tableofcontents[currentsection]
\end{frame}

\begin{frame}
  \frametitle{Heap Allocation}
  \begin{itemize}
    \item Step 1: use static allocation where possible
    \item Step 2: pick part of RAM and declare it stack
    \item Step 3: remainder of RAM is heap
  \end{itemize}
  \vskip5mm
  \begin{center}
    \begin{tikzpicture}
      \draw (0,0) rectangle ++(1,2);
      \draw (1,0) rectangle ++(2,2);
      \draw (3,0) rectangle ++(5,2);

      \node[rotate=90] at (0.5,1) {static};
      \node[rotate=90] at (2,1) {stack};
      \node[rotate=90] at (5.5,1) {heap};

      \draw[|-|] (0,-.5) -- ++(8,0) node[midway,below] {RAM};
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Heap Allocation}
  \notrealcpp
  \begin{columns}
    \column{5cm}
    \code[font size=\small,frame=none]{heap-allocation.cpp}
    \column{4cm}
    \begin{center}
      \begin{tikzpicture}[allocation/.style={-latex,thick},remember picture,overlay,scale=.5]
        \coordinate (start) at (-4,4);

        \only<handout:1-|3->{
          \draw[fill=red!50] (start) rectangle ++(4,-1);
        }
        \only<handout:1|3>{
          \draw[allocation] (var a) to [bend left=30] ($ (start) + (0,-.5) $);
        }

        \only<handout:1-|4->{
          \draw[fill=green!50] ($ (start) + (4,0) $) rectangle ++(4,-1);
        }
        \only<handout:1|4>{
          \draw[allocation] (var b) to [bend left=30] ($ (start) + (4,-0.5) $);
        }

        \only<handout:1-|5->{
          \draw[fill=blue!50] ($ (start) + (0,-1) $) rectangle ++(4,-1);
        }
        \only<handout:1|5>{
          \draw[allocation] (var c) to [bend left=30] ($ (start) + (0,-1.5) $);
        }

        \only<handout:1-|6->{
          \draw[fill=yellow!50] ($ (start) + (4,-1) $) rectangle ++(4,-1);
        }
        \only<handout:1|6>{
          \draw[allocation] (var d) to [bend left=30] ($ (start) + (4,-1.5) $);
        }

        \only<handout:2|7-11>{
          \draw[fill=red!50] ($ (start) + (0,-2) $) rectangle ++(8,-1);
        }
        \only<handout:2|7>{
          \draw[allocation] (mem a) to [bend right=30] ($ (start) + (0,-2.5) $);
        }

        \only<handout:2|8-10>{
          \draw[fill=green!50] ($ (start) + (0,-3) $) rectangle ++(8,-3);
        }
        \only<handout:2|8>{
          \draw[allocation] (mem b) to [bend right=30] ($ (start) + (0,-3.5) $);
        }

        \only<handout:2|9-12>{
          \draw[fill=blue!50] ($ (start) + (0,-6) $) rectangle ++(8,-2);
        }
        \only<handout:2|9>{
          \draw[allocation] (mem b) to [bend right=30] ($ (start) + (0,-6.5) $);
        }

        \only<handout:2|10-13>{
          \draw[fill=yellow!50] ($ (start) + (0,-8) $) rectangle ++(8,-2);
        }
        \only<handout:2|10>{
          \draw[allocation] (mem d) to [bend right=30] ($ (start) + (0,-8.5) $);
        }

        \only<handout:3|11>{
          \draw[->,thick] (del b) ++(2,0) -- ++(-1,0);
        }

        \only<handout:3|12>{
          \draw[->,thick] (del a) ++(2,0) -- ++(-1,0);
        }

        \only<handout:3|13>{
          \draw[->,thick] (del c) ++(2,0) -- ++(-1,0);
        }

        \only<handout:3|14>{
          \draw[->,thick] (del d) ++(2,0) -- ++(-1,0);
        }

        \draw (start) grid ++(8,-10);
      \end{tikzpicture}
    \end{center}
  \end{columns}
  \vskip2cm
  \begin{overprint}
    \onslide<handout:1|2-6>
    \begin{center}
      \textbf{Compilation stage} \\ Compiler assigns memory locations to each variable \\ (this is still static allocation)
    \end{center}

    \onslide<handout:2-3|7->
    \begin{center}
      \textbf{At runtime}
    \end{center}
  \end{overprint}
\end{frame}

\begin{frame}
  \frametitle{Fragmentation}
  \code{fragmentation.cpp}
  \begin{center}
    \begin{tikzpicture}[scale=.5]
      \foreach \i in {0,2,...,15} {
        \tikzmath{
          int \j;
          int \k;
          \j = int(\i + 2);
          \k = int(17 + int(\i / 2));
        }
        \only<\j-\k>{
          \draw[fill=red!50] (\i,0) rectangle ++(1,1);
        }
      }

      \foreach \i in {1,3,...,15} {
        \tikzmath{
          int \j;
          \j = int(\i + 2);
        }
        \only<\j-25>{
          \draw[fill=red!50] (\i,0) rectangle ++(1,1);
        }
      }

      \draw (0,0) grid (16,1);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Heap Allocation}
  \begin{procontralist}
    \pro Allows allocation of arbitrarily large blocks
    \pro Deallocation possible in any order
    \pro Most general allocation method
    \con Requires some bookkeeping \\ (keeps track of linked list of free blocks)
    \con Deallocation necessary
    \con Allocation/Deallocation relatively slow
    \con Fragmentation
  \end{procontralist}
\end{frame}

\begin{frame}
  \frametitle{Garbage Collection}
  \begin{center}
    \begin{tikzpicture}[language/.style={fill=red!50}]
      \node[anchor=south] at (0,.5) {\parbox{8cm}{\centering Memory allocation is too important to be left in the hands of machines}};
      \node at (0,0) {$\updownarrow$};
      \node[anchor=north] at (0,-.5) {\parbox{8cm}{\centering Memory allocation is too important to be left in the hands of humans}};

      \visible<2->{
        \node[language,anchor=south] at (0,2) {\cpp};
      }

      \visible<3->{
        \node[language,anchor=north] at (0,-2) {Java};
      }

      \visible<4->{
        \node[language,anchor=south] at (-2,2) {C};
      }

      \visible<5->{
        \node[language,anchor=south] at (2,2) {Pascal};
      }

      \visible<6->{
        \node[language,anchor=south] at (-4,2) {Cobol};
      }

      \visible<7->{
        \node[language,anchor=south] at (4,2) {PLI};
      }

      \visible<8->{
        \node[language,anchor=north] at (-2,-2) {C$\sharp$};
      }

      \visible<9->{
        \node[language,anchor=north] at (2,-2) {Python};
      }

      \visible<10->{
        \node[language,anchor=north] at (-4,-2) {Ruby};
      }

      \visible<11->{
        \node[language,anchor=north] at (4,-2) {Haskell};
      }

      \visible<12->{
        \node[language,anchor=north] at (0,-3) {Prolog};
      }

      \visible<13->{
        \node[language,anchor=north] at (-2,-3) {Lisp};
      }

      \visible<14->{
        \node[language,anchor=north] at (2,-3) {Basic};
      }

      \visible<15->{
        \node[language,anchor=north] at (-4,-3) {Perl};
      }

      \visible<16->{
        \node[language,anchor=north] at (4,-3) {Oz};
      }

      \visible<17->{
        \node[language,anchor=north] at (-1,-2.5) {F$\sharp$};
      }

      \visible<18->{
        \node[language,anchor=north] at (1,-2.5) {JavaScript};
      }

      \visible<19->{
        \node[language,anchor=north] at (-3,-2.5) {Clojure};
      }

      \visible<20->{
        \node[language,anchor=north] at (3,-2.5) {Erlang};
      }

      \visible<21->{
        \node[language,anchor=north] at (-5,-2.5) {Eiffel};
      }

      \visible<22->{
        \node[language,anchor=north] at (5,-2.5) {Smalltalk};
      }
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Garbage Collection}
  \begin{itemize}
    \item Abstraction of memory
    \item Tries to make it look as if there's an infinite amount of RAM
    \item Recycles unreachable memory
    \item Reduces memory leaks (but does not eliminate them)
    \item Many different techniques
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Allocation in Java}
  \begin{columns}[t]
    \column{4cm}
    \begin{center} \bf Static \end{center}
    \begin{itemize}
      \item Static fields
    \end{itemize}

    \column{4cm}
    \begin{center} \bf Stack \end{center}
    \begin{itemize}
      \item Parameter values
      \item Local variables
    \end{itemize}

    \column{4cm}
    \begin{center} \bf Heap \end{center}
    \begin{itemize}
      \item All objects
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Allocation in \cpp}
  \begin{columns}[t]
    \column{4cm}
    \begin{center} \bf Static \end{center}
    \begin{itemize}
      \item Global variables
      \item Static fields
      \item Static local variables
    \end{itemize}

    \column{4cm}
    \begin{center} \bf Stack \end{center}
    \begin{itemize}
      \item Parameter values
      \item Local variables
    \end{itemize}

    \column{4cm}
    \begin{center} \bf Heap \end{center}
    \begin{itemize}
      \item Anything created using {\tt new}
    \end{itemize}
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Java vs \cpp}
  \begin{itemize}
    \item \cpp\ is much more flexible
    \item Anything can be placed anywhere
  \end{itemize}
  \vskip5mm
  \begin{overprint}
    \onslide<handout:1|1>
    \structure{Example: statically allocated object}
    \code{object-on-static.cpp}

    \onslide<handout:2|2>
    \structure{Example: object on stack}
    \code{object-on-stack.cpp}

    \onslide<handout:3|3>
    \structure{Example: object on heap}
    \code{object-on-heap.cpp}

    \onslide<handout:4|4>
    \structure{Example: statically allocated integer}
    \code{int-on-static.cpp}

    \onslide<handout:5|5>
    \structure{Example: integer on stack}
    \code{int-on-stack.cpp}

    \onslide<handout:6|6>
    \structure{Example: integer on heap}
    \code{int-on-heap.cpp}
  \end{overprint}
\end{frame}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "allocation-methods"
%%% End:
